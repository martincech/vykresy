﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using NLog;

namespace vykresy
{
    public partial class MainWindow
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        const string DirFormat = "\\\\sr-db\\abraskladovekartyVyroba\\{0}";

        public MainWindow()
        {
            InitializeComponent();
            button.IsEnabled = false;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = History.List;
        }

        void listView_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var lv = e.Source as System.Windows.Controls.ListView;
            var item = (string)lv.Items.GetItemAt(lv.SelectedIndex);
            var itemProvider = new ItemProvider();
            var items = itemProvider.GetItems(string.Format(DirFormat, item));
            DataContext = items;
        }

        void directoryView_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = sender as TreeViewItem;
            if (item.DataContext is FileItem)
            {
                try
                {
                    using (var p = new Process
                    {
                        StartInfo = new ProcessStartInfo((item.DataContext as FileItem).Path)
                        {
                            RedirectStandardInput = false,
                            RedirectStandardError = false,
                            RedirectStandardOutput = false,
                            UseShellExecute = true
                        }
                    })
                    {
                        p.Start();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            History.Add(textBox.Text);
            listView.SelectedIndex = 0;
        }

        void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var folder = string.Format(DirFormat, textBox.Text);
            var folderExist = textBox.Text.Length > 0 ? Directory.Exists(folder) : false;

            textBox.Background =
                textBox.Text.Length == 0 ? SystemColors.ControlLightLightBrush :
                folderExist ?
                    new SolidColorBrush(new Color { R = 153, G = 180, B = 51, A = 100 }) :
                    new SolidColorBrush(new Color { R = 185, G = 29, B = 71, A = 100 });

            if (button != null)
            {
                button.IsEnabled = textBox.Text.Length > 0 && folderExist;
            }
        }
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && button.IsEnabled)
                button_Click(button, null);
            else
                base.OnKeyDown(e);
        }
    }
}
