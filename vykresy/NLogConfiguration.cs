﻿using System.Reflection;
using System.Runtime.InteropServices;
using NLog.LayoutRenderers;

namespace vykresy
{
    static class NLogConfiguration
    {
        static readonly string appName = Assembly.GetEntryAssembly().GetName().Name;
        static readonly string os = RuntimeInformation.OSDescription;
        static readonly string osArch = RuntimeInformation.OSArchitecture.ToString();
        static readonly string appArch = RuntimeInformation.ProcessArchitecture.ToString();
        static readonly string netFramework = RuntimeInformation.FrameworkDescription;

        public static void RegisterLayoutRenderers()
        {
            LayoutRenderer.Register("appName", logEvent => appName);
            LayoutRenderer.Register("os", logEvent => os);
            LayoutRenderer.Register("osArch", logEvent => osArch);
            LayoutRenderer.Register("appArch", logEvent => appArch);
            LayoutRenderer.Register("netFramework", logEvent => netFramework);
        }
    }

}
