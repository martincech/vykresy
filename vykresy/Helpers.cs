﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Data;
using NLog;

namespace vykresy
{
    [ValueConversion(typeof(object), typeof(string))]
    public class IconKindConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                          System.Globalization.CultureInfo culture)
        {
            var path = value as string;
            if (path == null)
                return "File";

            switch (Path.GetExtension(path).ToUpper())
            {
                case ".PDF":
                    return "FilePdf";
                case ".DWG":
                    return "FileCheck";
                case ".JPG":
                case ".JPEG":
                case ".PNG":
                case ".TIF":
                case ".TIFF":
                    return "FileImage";
                case ".XLSX":
                case ".XLS":
                    return "FileExcel";
                case ".DOCX":
                case ".DOC":
                case ".RTF":
                    return "FileWord";
                case ".PPTX":
                case ".PPT":
                    return "FilePowerpoint";
                case ".CSV":
                    return "FileDelimited";
                case ".EXE":
                    return "Application";
                case ".ZIP":
                case ".RAR":
                case ".7Z":
                    return "Archive";
                case ".MP4":
                case ".AVI":
                case ".MKV":
                    return "FileVideo";
                case ".XML":
                    return "FileXml";
                case ".LOG":
                case ".TXT":
                    return "FileDocument";
                default:
                    return "File";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                            System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    class Item
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }

    class FileItem : Item
    {
    }

    class DirectoryItem : Item
    {
        public List<Item> Items { get; set; }

        public DirectoryItem()
        {
            Items = new List<Item>();
        }
    }

    class ItemProvider : INotifyPropertyChanged
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        private string strPath;
        public event PropertyChangedEventHandler PropertyChanged;

        public ItemProvider()
        {
        }

        public ItemProvider(string strPath)
        {
            this.strPath = strPath;
        }

        public string PathString
        {
            get { return strPath; }
            set
            {
                strPath = value;
                OnPropertyChanged(nameof(PathString));
            }
        }

        protected void OnPropertyChanged(string strPath)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(strPath));
        }

        public List<Item> GetItems(string path)
        {
            var items = new List<Item>();
            try
            {
                var dirInfo = new DirectoryInfo(path);
                foreach (var directory in dirInfo.GetDirectories())
                {
                    items.Add(new DirectoryItem
                    {
                        Name = directory.Name,
                        Path = directory.FullName,
                        Items = GetItems(directory.FullName)
                    });
                }

                foreach (var file in dirInfo.GetFiles())
                {
                    items.Add(new FileItem
                    {
                        Name = file.Name,
                        Path = file.FullName
                    });
                }
                return items;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return items;
            }
        }
    }
}
