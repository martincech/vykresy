﻿using System.Windows;

namespace vykresy
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
            : base()
        {
            NLogConfiguration.RegisterLayoutRenderers();
        }
    }
}
