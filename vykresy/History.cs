﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json;
using NLog;

namespace vykresy
{
    class History
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        static readonly string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "VEIT", "vykresy", "history.json");

        static History()
        {
            try
            {
                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                var json = File.ReadAllText(path);
                var items = JsonConvert.DeserializeObject<List<string>>(json);
                items.ForEach(i => List.Add(i));
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        public static ObservableCollection<string> List { get; } = new ObservableCollection<string>();

        public static void Add(string item)
        {
            List.Insert(0, item);
            // keep last 50 entries
            while (List.Count > 50)
                List.RemoveAt(List.Count - 1);
            Save();
        }

        public static void Save()
        {
            try
            {
                File.WriteAllText(path, JsonConvert.SerializeObject(List));
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }
    }
}
